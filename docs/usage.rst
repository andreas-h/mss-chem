*******************
How to use MSS-Chem
*******************

Installation
============

Required dependencies
---------------------

* Python 2.7, 3.4, 3.5, or 3.6
* `numpy <http://www.numpy.org/>`__ (1.7 or later)
* `netcdf4-python <https://unidata.github.io/netcdf4-python/>`__


Instructions
------------

Using conda
~~~~~~~~~~~

MSS-Chem itself is a pure Python package, but its dependencies are not. The
easiest way to get everything installed is to use conda_. To install mss-chem
using the conda command line tool:

.. code:: shell

   $ conda install -c andreas-h mss-chem

.. _conda: http://conda.io/


Using pip
~~~~~~~~~

If you don’t use conda, be sure you have the required dependencies (see above)
installed first. Then, install mss-chem with pip:

.. code:: shell

   $ pip install mss-chem



Running MSS-Chem
================

::

   usage: msschem-dl [-h] (-m MODEL | -a) [-d DATE] [-c CONFIG] [-q | -v]
   
   MSS-Chem downloader
   
   optional arguments:
     -h, --help            show this help message and exit
     -m MODEL, --model MODEL
                           Model to download
     -a, --all             Download data from all configured models
     -d DATE, --date DATE  Date to download data for (YYYY-MM-DD)
     -c CONFIG, --config CONFIG
                           MSS-Chem configuration file
     -q, --quiet           No output except for errors
     -v, --verbosity       Increase output verbosity (can be supplied multiple times)

   
Configuration of MSS-Chem
=========================

The location of a configuration file can be passed to the ``msschem-dl`` script
with the ``--config`` option.  Alternatively, MSS-Chem tries to import a module
``msschem_settings``.  In that case, the file ``msschem_settings.py`` has to be
somewhere in your ``$PYTHONPATH``.

The configuration file could look like this:

.. literalinclude:: msschem_settings.py


Configuration of MSS
====================

The first MSS version to fully support the model data downloaded and prepared by
MSS-Chem is *MSS 1.6.2*.

MSS is configured via the file ``mss_wms_settings.py``, as described in the `MSS
documentation <https://mss.readthedocs.io/en/stable/deployment.html>`__.  To
include CTM data downloaded with MSS-Chem in the MSS WMS setup, the following
steps need to be taken:

- The path to the model data has to be specified in the ``datapath`` dict, e.g.,

  .. code:: python

     datapath = {
         "camsglobal": "/path/to/mss/data/camsg",
         "camsregional": "/path/to/mss/data/camsr",
         "silam": "/path/to/mss/data/silam",
     }

- The data classes have to be defined in the ``data`` dict, e.g.,

  .. code:: python

     data = {
         "camsglobal": mslib.mswms.dataaccess.DefaultDataAccess(datapath["camsglobal"], "ml"),
         "camsregional": mslib.mswms.dataaccess.DefaultDataAccess(datapath["camsregional"], "al"),
         "silam": mslib.mswms.dataaccess.DefaultDataAccess(datapath["silam"], "al"),
     }

- The MSS plotting styles have to be instantiated for both horizontal (maps) and
  vertical (cross-section) plots.  This is done using the ``make_msschem_class``
  functions from the modules `mslib.mswms.mpl_hsec_styles.py
  <https://bitbucket.org/wxmetvis/mss/src/stable/mslib/mswms/mpl_hsec_styles.py>`__
  and `mslib.mswms.mpl_vsec_styles.py
  <https://bitbucket.org/wxmetvis/mss/src/stable/mslib/mswms/mpl_vsec_styles.py>`__.

  For example, in order to instantiate (i.e., make available) plotting styles
  for model data on *altitude*, *pressure*, and *model* levels, one could do the
  following:

  .. code:: python

     # import plot style modules
     from mslib.mswms import mpl_hsec_styles
     from mslib.mswms import mpl_vsec_styles
     # import chemical species
     from mslib.mswms.msschem import MSSChemTargets

     # create horizontal (map) plot styles
     for vert in ["al", "pl", "ml"]:
         for stdname, props in list(MSSChemTargets.items()):
             name, qty, units, scale = props
             key = "HS_MSSChemStyle_" + vert.upper() + "_" + name + "_" + qty
             globals()[key] = mpl_hsec_styles.make_msschem_class(stdname, name, vert, units, scale)

     # create vertical (cross-section) plot styles
     for vert in ["al", "pl", "ml"]:
         for stdname, props in list(MSSChemTargets.items()):
             name, qty, units, scale = props
             key = "VS_MSSChemStyle_" + vert.upper() + "_" + name + "_" + qty
             globals()[key] = mpl_vsec_styles.make_msschem_class(stdname, name, vert, units, scale)

  These MSS *layer styles* are classes which follow the following naming
  scheme: ``XS_MSSChemStyle_LAYERTYPE_SPECIES_QUANTITY``.

  Here, 

  - ``XS`` is either ``HS`` for *horizontal* or ``VS`` for *vertical* plots
  - ``LAYERTYPE`` is one of ``AL``, ``PL``, ``ML`` for model data on *altitude*, *pressure*, and *model* levels, respectively,
  - ``SPECIES`` is the species to be plotted (in upper case)
  - ``QUANTITY`` identifies the quantity to be plotted, which can be one of
    ``mfrac`` for *mass fractions* (in kg/kg) and ``mconc`` for *mass
    concentrations* (in kg/m³)

  For example, the plotting style for horizontal plots of PM10 mass
  concentrations defined on model levels would be
  ``HS_MSSChemStyle_ML_PM10_mconc``.

- The horizontal plot styles which should be offered by the MSS WMS server have
  to be registered in the ``register_horizontal_layers`` list, e.g.,

  .. code:: python

     register_horizontal_layers = [
         (HS_MSSChemStyle_ML_NO2_mfrac, ["camsglobal"]),
         (HS_MSSChemStyle_AL_NO2_mconc, ["camsregional"]),
         (HS_MSSChemStyle_AL_PM10_mconc, ["silam"]),
     ]

- The vertical plot styles which should be offered by the MSS WMS server have to
  be registered in the ``register_horizontal_layers`` list, e.g.,

  .. code:: python

     register_vertical_layers = [
         (VS_MSSChemStyle_ML_NO2_mfrac, ["camsglobal"]),
         (VS_MSSChemStyle_AL_NO2_mconc, ["camsregional"]),
         (VS_MSSChemStyle_ALWithPressure_PM10_mconc, ["silam"]),
     ]
